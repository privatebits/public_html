---
layout: article
title:  "The Trouble With Passwords"
subtitle: "and why some of your accounts are likely no longer yours"
published-date:   2016-12-19 13:29:13 -0500
revised-date: 2016-12-19 13:29:13 -0500
permalink: /articles/the_trouble_with_passwords
tags: passwords security
---

You’ll find this post in your `_posts` directory. Go ahead and edit it and re-build the site to see your changes. You can rebuild the site in many different ways, but the most common way is to run `jekyll serve`, which launches a web server and auto-regenerates your site when a file is updated.

To add new posts, simply add a file in the `_posts` directory that follows the convention `YYYY-MM-DD-name-of-post.ext` and includes the necessary front matter. Take a look at the source for this post to get an idea about how it works.

There is no such thing as perfect security.

Security is fundamentally at odds with convenience.

The *value* of the stuff your passwords keep safe should be less than the *cost* of breaking the password.

### TL;DR: What you should do.

You, as a common user, are very average and are likely not worth anyone's time to attack directly, but take to protect yourself against automated attacks or in case a service you use falls victim. This can be challenging because you, as a user, have woefully little control over how your information, and your passwords, are used and stored. Some good practices:

Important:
- Do not use sketchy browser extensions. Be careful when reviewing permissions. Remember: you can deny these permissions! A browser extension that can read your tabs can read the passwords you type into those tabs.
- Likewise, do not install sketchy software.
- Do not re-use passwords. Use a password manager if possible. An HMAC-based password manager is more portable than a dictionary-based one.
- Keep your email and your phone number safe. These can be used to recover forgotten passwords, and are therefore.

The above guidelines are entirely insufficient to keep yourself safe while engaging in high-risk activity such as managing a high-risk identity (accounts belonging to organizations) or if you are somehow a person of interest, exemplified by whistleblowing, journalism in a hostile environment. We recommend you reach out to the EFF, and carefully consider your actions.

### The myth of strong passwords

#### Special characters: good intentions can do a lot of harm

What does the "password strength" indicator measure?
Threat model!
The "guessability" of a password is best measured by.. trying to guess it.
But it is challenging to compell an attacker to use only a specific technique.

#### A realistic threat model

Let's face it, you are very, very average. Your bank accounts
And your money is precious enough that you would surely notice if something seemed amiss.

The typical victim of financial crimes via any form of hacking

HaveIBeenPwned - use their API to check
iframe

### The secret lives of passwords: what happens after I create my account?

### Passwords are a shared secret

#### Salts, Hashes and other invisible ways companies forget to protect you.

(a one-way function).

(APPLET this is a program that runs in your browser. You can see it here)

An interesting consequence: many different passwords can log you into a website.
Example with SHA1

#### Password dumps: a rare glimpse into human lack of creativity
Hints "Password is pEnc1l123!" or worse yet, "same as my social".

### Authentication

Authentication is the act of a remote party (Facebook, for example) verifying that you are indeed you, at least you-enough to .
Terms such as "log in", "sign in", etcetera. are loosely used to describe the same. Authorization, however, happens behind the scenes and descibes the process by which a remote party (again, Facebook), decides whether you are allowed to see or do something based on your authenticated identity.

#### Something you know -
Passwords
Secret questions. These are tricky - we don't know how these are stored. These may leak a lot of information about you.
Social security numbers (USA) - why is this still a thing?! Selective service allows you to verify your guesses* - verify
Asymmetric cryptography.

#### Something you have -
Certificates.
Two Factor Authentication.
RSA token - synchronized secret sequence.
Google Authenticator
Password managers

#### Something you are - biometric
Interaction with the biometric sensor requires physical access, and that is a lot.
The sensor cannot tell the difference between something real and something fake.
Biometric data is not designed to be secret.
Sensors themselves are trusted and may be subverted or replaced. Unplug a fingerprint sensor, plug in something that records fingerprints of legitimate users and stores them for later use by the attacker.

### Forgotten passwords.

Security is at odds with convenience.
Reset password via email.
Reset password via phone call and secret questions.

### Be smarter about your use of passwords

### Be mindful of your identity, and how it is linked to you.
Your email? Your phone number? Your social security number?
A 2nd factor effectively links your identity via two means.

### Do not use the same password for multiple domains

### A pocket password generator

### HMAC-based password manager

### What you should do to protect yourself today

Two factor auth. Ideally via synchronized sequence.

Use two-factor authentication. Biometrics are a nice idea, but are simply Keep your 2FA device safe

Do not re-use passwords. Use a password manager.

Change passwords if you suspect wrongdoing.


[//]: # ( ## Future topics )
[//]: # (Rogaways *on the moral character of cryptographic work*. Discussion, quotes, etc )
[//]: # ("security", and why this word has been forever ruined by greed. Statement of security.  Threat model. Protected by military-grade encryption. Security-as-a-service)
[//]: # ( - What internet browsers do to keep you safe. Certificates. Same origin policy, Flash, etc. Security is fundamentally at odds with convenience. )
[//]: # ( - Anonymity online: why this is difficult and what is the best I can do? )
[//]: # ( - Whom do you trust?: Making sense of the colorful landscape of *secure* technology )
[//]: # ( - phishing: targeted and otherwise. Why the Nigerian prince wants to give you his inheritance )
[//]: # ( - private messaging: snapchat, Signal, WhatsApp and the rest. The devil is in the details. )
[//]: # ( - surveillance: nation states, advertisers, and other adversaries )
[//]: # ( - All you need to know about cryptography )

[//]: ( ### References)
[//]: ( [jekyll-docs]: http://jekyllrb.com/docs/home )
[//]: ( [jekyll-gh]:   https://github.com/jekyll/jekyll )
[//]: ( [jekyll-talk]: https://talk.jekyllrb.com/ )
