---
layout: tool
title: Encrypt or decrypt a message (AES)
subtitle: send secrets privately via untrusted means
permalink: /articles/
---

APPLET

Source:

### How this works

#### A quick look at the AES cryptosystem

The AES is a cipher (it makes noise from a message, noise from which the message can be recovered if and only if*

*up to a threat model

Diagram

Cryptography is difficult to implement correctly, so we use this very popular implementation of the SHA256 hash (@ commit # .....).

#### Derive an AES key from your password

AES requires an integer key

#### Hex-encode your message and break it into AES-sized chunks.

??? Escape the message to prevent someone from sending you malicious code as an encrypted message.
UTF8-encoded message, meaning each character is unambiguously encoded as a number according to the spec.
Pad with zeroes.

#### Encrypt the message via AES

Cryptography is difficult to implement correctly, so we use this very popular implementation of javascript (@ commit # .....).

#### Decryption

Derive a key from the password via a hash function.
For each X-bit block of the encrypted message
Decrypt with same AES implementation.
Re-encode the message to UTF8 from a hexadecimal string

#### What must I trust to use this tool?

You must trust the javascript on this page not to
You must trust your own computer (free of malicious software that could spy on your browser. Free of malicious browser extensions that could spy on this page).
Trust the AES cryptosystem to not be practrically broken.
